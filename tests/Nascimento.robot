*** Settings ***
Resource          ../driver/driver.resource
Resource          ../pages/nascimento.resource
Resource          ../keywords/keywords.resource
Test Setup        Abrir Navegador
Test Teardown     Fechar Navegador

*** Test Cases ***
Cenário 01 - Data de Nascimento Válida
    [Documentation]    Esse teste verifica se ocorreu sucesso ao tentar
    ...                inserir uma data de nascimento que seja válida.
    Dado que estou na página de data de nascimento
    Quando preencher o campo com data de nascimento válida
    E avançar para o proximo campo
    E clicar em "Continuar"
    Então valida-se o redirecionamento para a proxima página

Cenário 02 - Data de Nascimento Inválida
    [Documentation]    Esse teste verifica se ocorreu falha ao tentar
    ...                inserir uma data de nascimento que seja inválida.
    Dado que estou na página de data de nascimento
    Quando preencher o campo com data de nascimento inválida 10/15/1900
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "Data de nascimento inválida"

Cenário 03 - Data de Nascimento Menor de 18 Anos
    [Documentation]    Esse teste verifica se ocorreu sucesso ao tentar
    ...                inserir uma data de nascimento que seja menor de 18 anos.
    Dado que estou na página de data de nascimento
    Quando preencher o campo com data de nascimento menor de 18 anos
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "O cadastro é permitido para maiores de 18 anos e menores de 100 anos"

Cenário 04 - Data de Nascimento Obrigatória
    [Documentation]    Esse teste verifica se ocorreu validação de campo
    ...                ao tentar não inserir um inserir uma data de nascimento.
    Dado que estou na página de data de nascimento
    Quando não preencher o campo com data de nascimento
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "Campo obrigatório."