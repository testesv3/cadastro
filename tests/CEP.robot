*** Settings ***
Resource          ../driver/driver.resource
Resource          ../pages/cep.resource
Resource          ../keywords/keywords.resource
Test Setup        Abrir Navegador
Test Teardown     Fechar Navegador

*** Test Cases ***
Cenário 01 - CEP Válido
    [Documentation]    Esse teste verifica se ocorreu sucesso ao tentar
    ...                inserir um CEP que seja válido.
    Dado que estou na página de CEP
    Quando preencher o campo com CEP válido
    E avançar para o proximo campo
    E clicar em "Continuar"
    Então valida-se o redirecionamento para a proxima página

Cenário 02 - CEP Inválido
    [Documentation]    Esse teste verifica se ocorreu falha ao tentar
    ...                inserir um CEP que seja inválido.
    Dado que estou na página de CEP
    Quando preencher o campo com CEP inválido 99999999
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "CEP inválido ou inexistente na nossa base."

Cenário 03 - Nome Obrigatório
    [Documentation]    Esse teste verifica se ocorreu validação de campo
    ...                ao tentar não inserir um CEP.
    Dado que estou na página de CEP
    Quando não preencher o campo com CEP
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "Campo obrigatório."