*** Settings ***
Resource          ../driver/driver.resource
Resource          ../pages/nome.resource
Resource          ../keywords/keywords.resource
Test Setup        Abrir Navegador
Test Teardown     Fechar Navegador

*** Test Cases ***
Cenário 01 - Nome Válido
    [Documentation]    Esse teste verifica se ocorreu sucesso ao tentar
    ...                inserir um nome que seja válido.
    Dado que estou na página de nome
    Quando preencher o campo com nome válido
    E avançar para o proximo campo
    E clicar em "Continuar"
    Então valida-se o redirecionamento para a proxima página

Cenário 02 - Nome Inválido
    [Documentation]    Esse teste verifica se ocorreu falha ao tentar
    ...                inserir um nome que seja inválido.
    Dado que estou na página de nome
    Quando preencher o campo com nome inválido Céu
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "Nome inválido. Insira um nome e sobrenome válido."

Cenário 03 - Nome Obrigatório
    [Documentation]    Esse teste verifica se ocorreu validação de campo
    ...                ao tentar não inserir um nome.
    Dado que estou na página de nome
    Quando não preencher o campo com nome
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "Campo obrigatório."