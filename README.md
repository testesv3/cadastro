# Cadastro

Repositório com as suites de testes para a pagina V3 de cadastro.


## CPF

Nessa suite são testados 5 cenários distintos.

### Cenário 01 - CPF Válido
Esse teste verifica se ocorreu sucesso ao tentar inserir um CPF que seja válido.

### Cenário 02 - CPF Cadastrado
Esse teste verifica se ocorreu falha ao tentar inserir um CPF que já esteja cadastrado.

### Cenário 03 - CPF Inválido
Esse teste verifica se ocorreu falha ao tentar inserir um CPF que seja inválido.

### Cenário 04 - CPF Badpayers
Esse teste verifica se ocorreu falha ao tentar inserir um CPF que seja badpayer.

### Cenário 05 - CPF Obrigatório
Esse teste verifica se ocorreu validação de campo ao tentar não inserir um CPF.


## Nome

Nessa suite são testados 3 cenários distintos.

### Cenário 01 - Nome Válido
Esse teste verifica se ocorreu sucesso ao tentar inserir um nome que seja válido.

### Cenário 02 - Nome Inválido
Esse teste verifica se ocorreu falha ao tentar inserir um nome que seja inválido.

### Cenário 03 - Nome Obrigatório
Esse teste verifica se ocorreu validação de campo ao tentar não inserir um nome.


## Email

Nessa suite são testados 3 cenários distintos.

### Cenário 01 - Email Válido
Esse teste verifica se ocorreu sucesso ao tentar inserir um email que seja válido.

### Cenário 02 - Email Inválido
Esse teste verifica se ocorreu falha ao tentar inserir um email que seja inválido.

### Cenário 03 - Email Cadastrado
Esse teste verifica se ocorreu falha ao tentar inserir um email que seja inválido.


## CEP

Nessa suite são testados 3 cenários distintos.

### Cenário 01 - CEP Válido
Esse teste verifica se ocorreu sucesso ao tentar inserir um CEP que seja válido.

### Cenário 02 - CEP Inválido
Esse teste verifica se ocorreu falha ao tentar inserir um CEP que seja inválido.

### Cenário 03 - Nome Obrigatório
Esse teste verifica se ocorreu validação de campo ao tentar não inserir um CEP.


## Data de Nascimento

Nessa suite são testados 4 cenários distintos.

### Cenário 01 - Data de Nascimento Válida
Esse teste verifica se ocorreu sucesso ao tentar inserir uma data de nascimento que seja válida.

### Cenário 02 - Data de Nascimento Inválida
Esse teste verifica se ocorreu falha ao tentar inserir uma data de nascimento que seja inválida.

### Cenário 03 - Data de Nascimento Menor de 18 Anos
Esse teste verifica se ocorreu sucesso ao tentar inserir uma data de nascimento que seja menor de 18 anos.

### Cenário 04 - Data de Nascimento Obrigatória
Esse teste verifica se ocorreu validação de campo ao tentar não inserir um inserir uma data de nascimento.