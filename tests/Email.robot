*** Settings ***
Resource          ../driver/driver.resource
Resource          ../pages/email.resource
Resource          ../keywords/keywords.resource
Test Setup        Abrir Navegador
Test Teardown     Fechar Navegador

*** Test Cases ***
Cenário 01 - Email Válido
    [Documentation]    Esse teste verifica se ocorreu sucesso ao tentar
    ...                inserir um email que seja válido.
    Dado que estou na página de email
    Quando preencher o campo com email válido
    E avançar para o proximo campo
    E clicar em "Continuar"
    Então valida-se o redirecionamento para a proxima página

Cenário 02 - Email Inválido
    [Documentation]    Esse teste verifica se ocorreu falha ao tentar
    ...                inserir um email que seja inválido.
    Dado que estou na página de email
    Quando preencher o campo com email inválido teste
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "Este e-mail é inválido. Por favor, verifique se digitou corretamente ou utilize outro endereço de e-mail."

Cenário 03 - Email Cadastrado
    [Documentation]    Esse teste verifica se ocorreu falha ao tentar
    ...                inserir um email que seja inválido.
    Dado que estou na página de email
    Quando preencher o campo com email inválido Céu
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "Nome inválido. Insira um nome e sobrenome válido."