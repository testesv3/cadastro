*** Settings ***
Resource          ../driver/driver.resource
Resource          ../pages/cpf.resource
Resource          ../keywords/keywords.resource
Test Setup        Abrir Navegador
Test Teardown     Fechar Navegador

*** Test Cases ***
Cenário 01 - CPF Válido
    [Documentation]    Esse teste verifica se ocorreu sucesso ao tentar
    ...                inserir um CPF que seja válido.
    Dado que estou na página de CPF
    Quando preencher o campo com CPF válido
    E avançar para o proximo campo
    E clicar em "Você confirma que aceita os Termos e condições"
    E clicar em "Continuar"
    Então valida-se o redirecionamento para a proxima página

Cenário 02 - CPF Cadastrado
    [Documentation]    Esse teste verifica se ocorreu falha ao tentar
    ...                inserir um CPF que já esteja cadastrado.
    Dado que estou na página de CPF
    Quando preencher o campo com CPF cadastrado 12345678956
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "CPF Inválido. Informe um CPF válido."

Cenário 03 - CPF Inválido
    [Documentation]    Esse teste verifica se ocorreu falha ao tentar
    ...                inserir um CPF que seja inválido.
    Dado que estou na página de CPF
    Quando preencher o campo com CPF inválido 12345678956
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "Este CPF já está cadastrado."

Cenário 04 - CPF Badpayers
    [Documentation]    Esse teste verifica se ocorreu falha ao tentar
    ...                inserir um CPF que seja badpayer.
    Dado que estou na página de CPF
    Quando preencher o campo com CPF badpayer 12345678956
    E avançar para o proximo campo
    E clicar em "Você confirma que aceita os Termos e condições"
    E clicar em "Continuar"
    Então janela de aviso deve carregar

Cenário 05 - CPF Obrigatório
    [Documentation]    Esse teste verifica se ocorreu validação de campo
    ...                ao tentar não inserir um CPF.
    Dado que estou na página de CPF
    Quando não preencher o campo com CPF
    E avançar para o proximo campo
    Então valida-se exibição de mensagem de erro "Campo obrigatório."